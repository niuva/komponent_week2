'use strict';
const PORT = 8000;
const HOST = '0.0.0.0';
const express = require('express');
const bodyParser = require('body-parser');
const Student = require('./student.js'); //Viitataan student schemaan student.js -tiedostossa
var mongoose = require('mongoose');
var socket = express();
mongoose.connect('mongodb://localhost/testi:27017'); //Yhdistetään kontin mongo tietokantaan
socket.use(bodyParser.urlencoded({ extended: true }));
socket.use(bodyParser.json());

//REST API - Haetaan ID:lla student
socket.get('/api/v1/getstudent/:id', function(req, res) {
    var id = req.params.id;
    Student.findOne({ id: id }) //Etsitaan student rest apista saadulla id:lla
        .then(function(student) {
            if (!student) //Jos ei natsaa, heitetaan erroria
                throw new Error("COULD NOT FIND STUDENT");
            res.send(student)
        })
        .catch(function(error) {
            res.send(error.message);
        })
});

//REST API - Lisätään student
socket.post('/api/v1/addstudent', function(req, res) {
    var student = new Student({ //Nama on maarritelty student.js studentSchemassa
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        nickname: req.body.nickname,
        email: req.body.email,
        id: req.body.id
    });
    student.save()
        .then(function(student) {
            if (!student) //Jos ei natsaa, heitetaan erroria
                throw new Error("ERROR HAS OCCURRED");
            res.send("STUDENT ADDED SUCCESSFULLY"); //Ilmoitetaan rest apin kautta onnistumisesta
        })
        .catch(function(error) {
            res.send(error.message);
        })
})

//REST API - Poistetaan ID:lla student (copy paste getstudentista)
socket.delete('/api/v1/removestudent/:id', function(req, res) {
    var id = req.params.id;
    Student.remove({ id: id }) //Poistetaan student rest apista saadulla id:lla
        .then(function(student) {
            if (!student) //Jos ei natsaa, heitetaan erroria
                throw new Error("ERROR HAS OCCURRED");
            res.send(id)
        })
})

//Kun palvelin kaynnissa, logiin vahvistus
socket.listen(PORT, function() {
    console.log("LISTENING " + HOST + ":" + PORT);
})