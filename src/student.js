'use strict';
var mongoose = require('mongoose');

var studentSchema = mongoose.Schema({
    firstname: String,
    lastname: String,
    nickname: String,
    email: String,
    id: Number
})

var Student = mongoose.model('Student', studentSchema)

module.exports = mongoose.model('Student', studentSchema);